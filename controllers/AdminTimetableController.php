<?php

namespace app\controllers;

use app\models\TimetableClassesSelect;
use Yii;
use app\models\Timetable;
use app\models\TimetableSearch;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TimetableController implements the CRUD actions for Timetable model.
 */
class AdminTimetableController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timetable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model= new TimetableClassesSelect();
        $day1= Timetable::find()->where(['day'=>1])->orderBy('sort')->all();
        if ($model->load(Yii::$app->request->post())) {
            $day1= Timetable::find()->where(['day'=>1])->where(['classes_id'=>$model->classes_id])->orderBy('sort')->all();
        }
        return $this->render('index', [
            'model'=>$model,
            'day1'=>$day1,
        ]);
    }
    /**
     * Displays a single Timetable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEdit($day,$classes_id)
    {
        $query=Timetable::find()->where(['day'=>$day])->andWhere(['classes_id'=>$classes_id])
            ->orderBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('edit', [
            'dataProvider' => $dataProvider,
            'day'=> $day,
            'classes_id'=> $classes_id,
        ]);
    }
    /**
     * Displays a single Timetable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddSubjectToTimetable($day,$classes_id)
    {
        $model= new Timetable();
        $model->classes_id=$classes_id;
        $model->day=$day;
        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['admin-timetable/edit','day'=>$day,'classes_id'=>$classes_id]);
        }
        return $this->render('create',['model'=>$model]);
    }

    /**
     * Creates a new Timetable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Timetable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Timetable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Timetable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $day=$model->day;
        $classes_id=$model->classes_id;
        $model->delete();

        return $this->redirect(['admin-timetable/edit','day'=>$day,'classes_id'=>$classes_id]);
    }

    /**
     * Finds the Timetable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timetable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timetable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
