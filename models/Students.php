<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "students".
 *
 * @property int $id
 * @property string $firstname Імʼя
 * @property string $lastname Прізвище
 * @property int $classes_id Клас
 *
 * @property Marks[] $marks
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname',], 'required'],
            [['classes_id'], 'integer'],
            [['firstname', 'lastname'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Імʼя',
            'lastname' => 'Прізвище',
            'classes_id' => 'Клас',
        ];
    }

    /**
     * Gets query for [[marks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasMany(Marks::className(), ['student_id' => 'id']);
    }
    public static function getStudentList()
    {
        return ArrayHelper::map(self::find()->all(),'id','lastname');
    }
}
