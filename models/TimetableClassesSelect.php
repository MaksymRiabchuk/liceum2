<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class TimetableClassesSelect extends Model
{
    public $classes_id=1;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['classes_id',], 'required'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'classes_id' => 'Виберіть клас',
        ];
    }
}
