<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subjects".
 *
 * @property int $id
 * @property string $name Назва предмета
 * @property string|null $url Посилання на підручник
 * @property int $classes_id Клас
 *
 * @property Marks[] $marks
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subjects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'classes_id'], 'required'],
            [['name'], 'string', 'max' => 32],
            [['url'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва предмета',
            'url' => 'Посилання на підручник',
            'classes_id' => 'Клас',
        ];
    }

    /**
     * Gets query for [[marks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasMany(Marks::className(), ['subject_id' => 'id']);
    }
    public static function getSubjectList()
    {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
}
