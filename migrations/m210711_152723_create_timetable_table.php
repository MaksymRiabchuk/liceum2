<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%timetable}}`.
 */
class m210711_152723_create_timetable_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%timetable}}', [
            'id' => $this->primaryKey(),
            'classes_id'=> $this->integer(),
            'subjects_id'=> $this->integer(),
            'teachers_id'=> $this->integer(),
            'sort'=> $this->integer(),
            'day'=> $this->smallInteger(),
        ]);
        $this->addForeignKey('fk-timetable-classes_id-classes-id','{{%timetable}}',
            'classes_id','{{%classes}}','id');
        $this->addForeignKey('fk-timetable-subjects_id-subjects-id','{{%timetable}}',
            'subjects_id','{{%subjects}}','id');
        $this->addForeignKey('fk-timetable-teachers_id-teachers-id','{{%timetable}}',
            'teachers_id','{{%teachers}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-timetable-classes_id-classes-id','{{%timetable}}');
        $this->dropForeignKey('fk-timetable-subjects_id-subjects-id','{{%timetable}}');
        $this->dropForeignKey('fk-timetable-teachers_id-teachers-id','{{%timetable}}');
        $this->dropTable('{{%timetable}}');
    }
}
