<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%students}}`.
 */
class m210606_174324_create_students_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%students}}', [
            'id' => $this->primaryKey(),
            'firstname'=> $this->string(32)->notNull()->comment('Імʼя'),
            'lastname'=> $this->string(32)->notNull()->comment('Прізвище'),
            'form'=> $this->integer(2)->notNull()->comment('Клас'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%students}}');
    }
}
