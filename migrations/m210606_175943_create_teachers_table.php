<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%teachers}}`.
 */
class m210606_175943_create_teachers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%teachers}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(32)->notNull()->comment('Імʼя вчителя'),
            'surname' => $this->string(32)->notNull()->comment('Прізвище вчителя'),
            'lastname' => $this->string(32)->notNull()->comment('По батькові вчителя'),
            'subject' => $this->string(255)->notNull()->comment('Предмет ведення')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%teachers}}');
    }
}
