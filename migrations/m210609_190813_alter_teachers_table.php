<?php

use yii\db\Migration;

/**
 * Class m210609_190813_alter_teachers_table
 */
class m210609_190813_alter_teachers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(\app\models\Teachers::tableName(),'subject');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(\app\models\Teachers::tableName(),'subject', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210609_190813_alter_teachers_table cannot be reverted.\n";

        return false;
    }
    */
}
