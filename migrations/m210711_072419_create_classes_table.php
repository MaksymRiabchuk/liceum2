<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%classes}}`.
 */
class m210711_072419_create_classes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%classes}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
        ]);
        $this->dropColumn('{{%students}}','form');
        $this->addColumn('{{%students}}','classes_id', $this->integer());
        $this->addForeignKey('fk-students-classes_id-classes-id','{{%students}}',
            'classes_id','{{%classes}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-students-classes_id-classes-id','{{%students}}');
        $this->dropColumn('{{%students}}','classes_id');
        $this->addColumn('{{%students}}','form', $this->integer(2)->notNull()->comment('Клас'));
        $this->dropTable('{{%classes}}');
    }
}
