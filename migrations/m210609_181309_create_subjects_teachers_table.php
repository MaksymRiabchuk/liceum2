<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subjects_teachers}}`.
 */
class m210609_181309_create_subjects_teachers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%subjects_teachers}}', [
            'teacher_id'=> $this->integer(),
            'subject_id'=> $this->integer(),
        ]);
        $this->addPrimaryKey('pk-teacher-subject','{{%subjects_teachers}}',['teacher_id','subject_id']);
        $this->addForeignKey('fk_subject_id2','{{%subjects_teachers}}','subject_id','{{%subjects}}','id');
        $this->addForeignKey('fk_teacher_id2','{{%subjects_teachers}}','teacher_id','{{%teachers}}','id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%subjects_teachers}}');
    }
}