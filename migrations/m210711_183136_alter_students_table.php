<?php

use yii\db\Migration;

/**
 * Class m210711_183136_alter_students_table
 */
class m210711_183136_alter_students_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%subjects}}','form');
        $this->addColumn('{{%subjects}}','classes_id', $this->integer());
        $this->addForeignKey('fk-subjects-classes_id-classes-id','{{%subjects}}',
            'classes_id','{{%classes}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-subjects-classes_id-classes-id','{{%subjects}}');
        $this->dropColumn('{{%subjects}}','classes_id');
        $this->addColumn('{{%subjects}}','form', $this->integer(2)->notNull()->comment('Клас'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210711_183136_alter_students_table cannot be reverted.\n";

        return false;
    }
    */
}
