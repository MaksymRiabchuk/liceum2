<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%marks}}`.
 */
class m210606_180722_create_marks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%marks}}', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer()->notNull()->comment('Учня'),
            'subject_id' => $this->integer()->notNull()->comment('Предмет'),
            'teacher_id' => $this->integer()->notNull()->comment('Учитель'),
            'mark' => $this->integer()->notNull()->comment('Оцінка')


        ]);
        $this->addForeignKey('fk_student_id','{{%marks}}','student_id','{{%students}}','id');
        $this->addForeignKey('fk_subject_id','{{%marks}}','subject_id','{{%subjects}}','id');
        $this->addForeignKey('fk_teacher_id','{{%marks}}','teacher_id','{{%teachers}}','id');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%marks}}');
    }
}
