<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeachersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вчителі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teachers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавити вчителя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'surname',
            'lastname',
            ['label'=> 'Предмети',
                'value'=> function($data){
                $subjectList='';
                foreach ($data->subjects as $subject)
                {
                    $subjectList.=$subject->name.PHP_EOL;
                }
                return $subjectList;
            }],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
