<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Teachers */

$this->title = 'Добавити вчителя';
$this->params['breadcrumbs'][] = ['label' => 'Список вчителів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teachers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
