<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Teachers */

$this->title = 'Редагування вчителя: ' . $model->surname.' '. $model->firstname.' '. $model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Список вчителів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->surname.' '. $model->firstname.' '. $model->lastname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="teachers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
