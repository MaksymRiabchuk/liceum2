<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subjects */

$this->title = 'Список предметів';
$this->params['breadcrumbs'][] = ['label' => 'Список предметів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
