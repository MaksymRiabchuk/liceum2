<?php

/* @var $this yii\web\View */
/* @var $teacher \app\models\Teachers */
/* @var $teachers array */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список вчителів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $teachers,
        'columns' => [
            'firstname',
            'surname',
            'lastname',
            'subject',
        ],
    ]) ?>
</div>
