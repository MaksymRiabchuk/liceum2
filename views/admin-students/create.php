<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Students */

$this->title = 'Добавити учня';
$this->params['breadcrumbs'][] = ['label' => 'Список учнів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
