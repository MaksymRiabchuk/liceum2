<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Students */

$this->title = 'Редагування учня: ' . $model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Список учнів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування учня';
?>
<div class="students-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
