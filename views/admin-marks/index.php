<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оцінки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавити оцінку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'student_id',
                'filter'=>\app\models\Students::getStudentList(),
                'value'=>function($data){
                    return $data->student->firstname.' '. $data->student->lastname;
                }
            ],
            ['attribute'=>'subject_id',
                'filter'=>\app\models\Subjects::getSubjectList(),
                'value'=>function($data){
                    return $data->subject->name;
                }
            ],
            ['attribute'=>'teacher_id',
                'filter'=>\app\models\Teachers::getTeacherList(),
                'value'=>function($data){
                    return $data->teacher->firstname.' '. $data->teacher->lastname;
                }
            ],
            'mark',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
