<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */
$this->title = 'Редагування учня: ' . $model->student->firstname.' '.$model->student->lastname ;
$this->params['breadcrumbs'][] = ['label' => 'Список оцінок', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->student->firstname.' '.$model->student->lastname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="marks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
