<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marks-form">
    <div class="panel panel-default">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'student_id')->dropDownList(\app\models\Students::getStudentList()) ?>

            <?= $form->field($model, 'subject_id')->dropDownList(\app\models\Subjects::getSubjectList()) ?>

            <?= $form->field($model, 'teacher_id')->dropDownList(\app\models\Teachers::getTeacherList()) ?>

            <?= $form->field($model, 'mark')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>