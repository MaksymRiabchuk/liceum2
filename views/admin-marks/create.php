<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */

$this->title = 'Добавити оцінку';
$this->params['breadcrumbs'][] = ['label' => 'Список оцінок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
