<?php

/* @var $this yii\web\View */
/* @var $student \app\models\Students */
/* @var $students array */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список учнів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $students,
        'columns' => [
            'firstname',
            'lastname',
            'form',
        ],
    ]) ?>
</div>
