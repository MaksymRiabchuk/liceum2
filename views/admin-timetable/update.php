<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Timetable */

$this->title = 'Редагування розкладу: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Розклад', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="timetable-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
