<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Timetable */
/* @var $form yii\widgets\ActiveForm */
$subjectList=\app\models\Subjects::getSubjectList();
$teacherList=\app\models\Teachers::getTeacherList();
?>

<div class="timetable-form">
    <div class="panel panel-default">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'classes_id')->hiddenInput()->label(false)  ?>

    <?= $form->field($model, 'subjects_id')->dropDownList($subjectList)?>

    <?= $form->field($model, 'teachers_id')->dropDownList($teacherList) ?>

    <?= $form->field($model, 'sort')->hiddenInput() ->label(false) ?>

    <?= $form->field($model, 'day')->hiddenInput()->label(false)  ?>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
