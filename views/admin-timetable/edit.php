<?php
/* @var $model \app\models\Timetable
 * @var $day integer
 * @var $classes_id integer
 */

use app\models\Classes;
use app\models\Subjects;
use app\models\Teachers;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = 'Редагування розкладу';
$classesList = Classes::getClassList();
$subjectList= Subjects::getSubjectList();
$teacherList= Teachers::getTeacherList()
?>
<div class="subjects-form">
    <div class="panel panel-default">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= Html::a('Додати',
                        ['admin-timetable/add-subject-to-timetable', 'day' => $day, 'classes_id' => $classes_id],
                        ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <div class="row">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute'=>'classes_id',
                            'value'=>function($model) use ($classesList){
                                return $classesList[$model->classes_id];
                            }
                        ],
                        [
                            'attribute'=>'subjects_id',
                            'value'=>function($model) use ($subjectList){
                                return isset ($subjectList[$model->subjects_id])? $subjectList[$model->subjects_id]: 'Не задано' ;
                            }
                        ],
                        [
                            'attribute'=>'teachers_id',
                            'value'=>function($model) use ($teacherList){
                                return isset ($teacherList[$model->teachers_id])? $teacherList[$model->teachers_id]: 'Не задано' ;
                            }
                        ],

                        ['class' => 'yii\grid\ActionColumn',
                            'template'=>'{update} {delete}'
                        ],
                    ],
                ]); ?>
            </div>


        </div>


    </div>
</div>
