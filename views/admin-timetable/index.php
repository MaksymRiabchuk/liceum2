<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Розклад';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timetable-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
    ]); ?>
    <div class="row">
        <div class="col-md-11">
            <?= $form->field($model, 'classes_id')->dropDownList(\app\models\Classes::getClassList()) ?>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <?= Html::submitButton('Вибрати', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><span style="font-size: 24px">Понеділок</span>
                    <?=Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                        ['admin-timetable/edit','day'=>'1','classes_id'=>$model->classes_id],
                        ['class'=>'btn btn-success pull-right'])?>

                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                    <?php /* @var $day \app\models\Timetable */?>
                    <?php foreach ($day1 as $day):?>
                    <tr>
                        <td><?=isset($day->subjects->name)? $day->subjects->name: 'Пусто' ?></td>
                        <td><?=isset($day->teachers->firstname)? $day->teachers->firstname.' '.
                                $day->teachers->lastname: 'Пусто' ?>
                        </td>

                    </tr>
                    <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><span style="font-size: 24px">Вівторок</span>
                    <?=Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                        ['admin-timetable/edit','day'=>'2','classes_id'=>$model->classes_id],
                        ['class'=>'btn btn-success pull-right'])?>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <?php /* @var $day \app\models\Timetable */?>
                        <?php foreach ($day1 as $day):?>
                            <tr>
                                <td><?=isset($day->subjects->name)? $day->subjects->name: 'Пусто' ?></td>
                                <td><?=isset($day->teachers->firstname)? $day->teachers->firstname.' '.
                                        $day->teachers->lastname: 'Пусто' ?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><span style="font-size: 24px">Середа</span>
                    <?=Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                        ['admin-timetable/edit','day'=>'3','classes_id'=>$model->classes_id],
                        ['class'=>'btn btn-success pull-right'])?>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <?php /* @var $day \app\models\Timetable */?>
                        <?php foreach ($day1 as $day):?>
                            <tr>
                                <td><?=isset($day->subjects->name)? $day->subjects->name: 'Пусто' ?></td>
                                <td><?=isset($day->teachers->firstname)? $day->teachers->firstname.' '.
                                        $day->teachers->lastname: 'Пусто' ?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><span style="font-size: 24px">Четвер</span>
                    <?=Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                        ['admin-timetable/edit','day'=>'4','classes_id'=>$model->classes_id],
                        ['class'=>'btn btn-success pull-right'])?>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <?php /* @var $day \app\models\Timetable */?>
                        <?php foreach ($day1 as $day):?>
                            <tr>
                                <td><?=isset($day->subjects->name)? $day->subjects->name: 'Пусто' ?></td>
                                <td><?=isset($day->teachers->firstname)? $day->teachers->firstname.' '.
                                        $day->teachers->lastname: 'Пусто' ?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><span style="font-size: 24px">П'ятниця</span>
                    <?=Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                        ['admin-timetable/edit','day'=>'5','classes_id'=>$model->classes_id],
                        ['class'=>'btn btn-success pull-right'])?>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <?php /* @var $day \app\models\Timetable */?>
                        <?php foreach ($day1 as $day):?>
                            <tr>
                                <td><?=isset($day->subjects->name)? $day->subjects->name: 'Пусто' ?></td>
                                <td><?=isset($day->teachers->firstname)? $day->teachers->firstname.' '.
                                        $day->teachers->lastname: 'Пусто' ?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
