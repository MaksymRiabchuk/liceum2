<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Timetable */

$this->title = 'Добавити в розклад';
$this->params['breadcrumbs'][] = ['label' => 'Розклад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timetable-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
