<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<div>
<div class="container">
    <?= Yii::$app->user->isGuest ? Html::img('/img/personal2.png'): ''?>
    <?php
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            [
                'label' => 'Учні',
                'url' => ['/students/index'],
                'visible' => Yii::$app->user->isGuest],
            [
                'label' => 'Класи',
                'url' => ['/classes/index'],
                'visible' => Yii::$app->user->isGuest
            ],
            [
                'label' => 'Розклад',
                'url' => ['/timetable/index'],
                'visible' => Yii::$app->user->isGuest
            ],
            [
                'label' => 'Предмети',
                'url' => ['/subjects/index'],
                'visible' => Yii::$app->user->isGuest
            ],
            [
                'label' => 'Вчителі',
                'url' => ['/teachers/index'],
                'visible' => Yii::$app->user->isGuest
            ],
            [
                'label' => 'Оцінки',
                'url' => ['/marks/index'],
                'visible' => Yii::$app->user->isGuest
            ],
            [
                'label' => 'Учні(Адмін)',
                'url' => ['/admin-students/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            [
                'label' => 'Класи(Адмін)',
                'url' => ['/admin-classes/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            [
                'label' => 'Розклад(Адмін)',
                'url' => ['/admin-timetable/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            [
                'label' => 'Предмети(Адмін)',
                'url' => ['/admin-subjects/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            [
                'label' => 'Вчителі(Адмін)',
                'url' => ['/admin-teachers/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            [
                'label' => 'Оцінки(Адмін)',
                'url' => ['/admin-marks/index'],
                'visible' => !Yii::$app->user->isGuest
            ],
            Yii::$app->user->isGuest ? (
            ['label' => ' Вхід', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Вихід (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    ?>
</div>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="clearfix">
    <div class="container">
    <div>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    </div>
    </div>
</div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy;Моя компанія <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
