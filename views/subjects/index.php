<?php

/* @var $this yii\web\View */
/* @var $subject \app\models\Subjects */

/* @var $subjects array */

use app\models\Classes;
use yii\grid\GridView;
use yii\helpers\Html;
$classesList = Classes::getClassList();
$this->title = 'Список предметів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $subjects,
        'columns' => [
            'name',
            ['attribute' => 'url',

                'format' => 'raw',
                'value' => function ($data) {
                    return $data->url == '' ? '' : Html::a('download', $data->url);
                }
            ],
            ['attribute' => 'classes_id',
                'filter' =>\app\models\Classes::getClassList(),
                    'value'=>function($model) use ($classesList){
                        return $classesList[$model->classes_id];
                }
            ],
        ],
    ]) ?>
</div>
