<?php

/* @var $this yii\web\View */
/* @var $mark \app\models\Students */
/* @var $marks array */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список оцінок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $marks,
        'columns' => [
            ['attribute'=>'student_id',
                'filter'=>\app\models\Students::getStudentList(),
                'value'=>function($data){
                    return $data->student->firstname.' '. $data->student->lastname;
                },
            ],
            ['attribute'=>'subject_id',
                'filter'=>\app\models\Subjects::getSubjectList(),
                'value'=>function($data){
                    return $data->subject->name;
                }
            ],
            ['attribute'=>'teacher_id',
                'filter'=>\app\models\Teachers::getTeacherList(),
                'value'=>function($data){
                    return $data->teacher->firstname.' '. $data->teacher->lastname;
                }
            ],
            'mark',
        ],
    ]) ?>
</div>
